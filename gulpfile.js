var gulp = require('gulp');
var gitDeploy = require('gulp-git-deploy');


gulp.task('build', function(){
 //your build stuff.
})

gulp.task('deploy',function(){

  return gitDeploy({remote: 'origin', name: 'master'}, function(){
    //put here whatever you want to do after merging, usually a build task.
    gulp.start('build')
  });

})
